﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KalkulatorWarungV2
{
    public partial class KalkulatorWarungV2 : Form
    {
        bool op_tekan = false;
        double nilai = 0;
        string ops = "";
        string number1, number2;

        public KalkulatorWarungV2()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn0_Click(object sender, EventArgs e)
        {

        }

        private void btn_click(object sender, EventArgs e)
        {
            if ((txtHasil.Text == "0") || op_tekan)
                txtHasil.Text = "";
            op_tekan = false;

            Button b = (Button)sender;
            txtHasil.Text = txtHasil.Text + b.Text;
        }

        private void operationClick(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (nilai != 0)
            {
                btnSamaDengan.PerformClick();
                ops = b.Text;
                nilai = Double.Parse(txtHasil.Text);
                labelHasil.Text = nilai + " " + ops;
                op_tekan = true;
            } else
            {
                ops = b.Text;
                nilai = Double.Parse(txtHasil.Text);
                labelHasil.Text = nilai + " " + ops;
                op_tekan = true;
            }

            number1 = labelHasil.Text;
        }

        private void hitungClick(object sender, EventArgs e)
        {
            number2 = txtHasil.Text;
            switch (ops)
            {
                case "+":
                    txtHasil.Text = (nilai + Double.Parse(txtHasil.Text)).ToString();
                    break;
                case "-":
                    txtHasil.Text = (nilai - Double.Parse(txtHasil.Text)).ToString();
                    break;
                case "×":
                    txtHasil.Text = (nilai * Double.Parse(txtHasil.Text)).ToString();
                    break;
                case "/":
                    txtHasil.Text = (nilai / Double.Parse(txtHasil.Text)).ToString();
                    break;
                default:
                    ops = "no";
                    break;
            }
            nilai = Double.Parse(txtHasil.Text);
            btnHapusHistory.Visible = true;
            op_tekan = true;

            if(ops != "no")
            {
                // Append history to text box
                labelHistory.Text = "";
                rtbHistory.AppendText(number1 + " " + number2 + "  =  " + "\n");
                rtbHistory.AppendText("\n       " + txtHasil.Text + "\n\n\n");
            }
            
            //Re-initiate
            labelHasil.Text = "";
            ops = "";
            nilai = 0;
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            txtHasil.Text = txtHasil.Text.Remove(txtHasil.Text.Length - 1, 1);
            if (txtHasil.Text.Length == 0)
            {
                txtHasil.Text = "0";
            }
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            txtHasil.Text = "0";
            nilai = 0;
            labelHasil.Text = "";
        }

        private void btnPersen_Click(object sender, EventArgs e)
        {
            txtHasil.Text = (Double.Parse(txtHasil.Text) * 0.01).ToString();
        }

        private void txtHasil_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnPlusMinus_Click(object sender, EventArgs e)
        {
            if (!txtHasil.Text.Contains("-"))
                txtHasil.Text = "-" + txtHasil.Text;
            else
                txtHasil.Text = txtHasil.Text.Trim('-');
        }

        private void btnHapusHistory_Click(object sender, EventArgs e)
        {
            rtbHistory.Text = "";
            if(labelHistory.Text == "")
            {
                labelHistory.Text= "Tidak ada history";
            }
        }

        private void btnKoma_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (b.Text == ".")
            {
                if (!txtHasil.Text.Contains("."))
                    txtHasil.Text = txtHasil.Text + b.Text;
            }
            else
                txtHasil.Text = txtHasil.Text + b.Text;
        }
    }
}
