﻿namespace KalkulatorWarungV2
{
    partial class KalkulatorWarungV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KalkulatorWarungV2));
            this.btnSamaDengan = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnKoma = new System.Windows.Forms.Button();
            this.btnKurang = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btnKali = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btnBagi = new System.Windows.Forms.Button();
            this.btnPersen = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.txtHasil = new System.Windows.Forms.TextBox();
            this.labelHasil = new System.Windows.Forms.Label();
            this.btnC = new System.Windows.Forms.Button();
            this.btnPlusMinus = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbHistory = new System.Windows.Forms.RichTextBox();
            this.labelHistory = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnHapusHistory = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSamaDengan
            // 
            this.btnSamaDengan.BackColor = System.Drawing.Color.DarkOrange;
            this.btnSamaDengan.FlatAppearance.BorderSize = 0;
            this.btnSamaDengan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSamaDengan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnSamaDengan.ForeColor = System.Drawing.Color.White;
            this.btnSamaDengan.Location = new System.Drawing.Point(308, 450);
            this.btnSamaDengan.Name = "btnSamaDengan";
            this.btnSamaDengan.Size = new System.Drawing.Size(80, 59);
            this.btnSamaDengan.TabIndex = 42;
            this.btnSamaDengan.Text = "=";
            this.btnSamaDengan.UseVisualStyleBackColor = false;
            this.btnSamaDengan.Click += new System.EventHandler(this.hitungClick);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.Color.White;
            this.btn0.FlatAppearance.BorderSize = 0;
            this.btn0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn0.ForeColor = System.Drawing.Color.DimGray;
            this.btn0.Location = new System.Drawing.Point(144, 450);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(80, 59);
            this.btn0.TabIndex = 41;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btn_click);
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.White;
            this.btnTambah.FlatAppearance.BorderSize = 0;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTambah.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnTambah.ForeColor = System.Drawing.Color.DimGray;
            this.btnTambah.Location = new System.Drawing.Point(308, 389);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(80, 59);
            this.btnTambah.TabIndex = 40;
            this.btnTambah.Text = "+";
            this.btnTambah.UseVisualStyleBackColor = false;
            this.btnTambah.Click += new System.EventHandler(this.operationClick);
            // 
            // btnKoma
            // 
            this.btnKoma.BackColor = System.Drawing.Color.White;
            this.btnKoma.FlatAppearance.BorderSize = 0;
            this.btnKoma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKoma.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnKoma.ForeColor = System.Drawing.Color.DimGray;
            this.btnKoma.Location = new System.Drawing.Point(226, 450);
            this.btnKoma.Name = "btnKoma";
            this.btnKoma.Size = new System.Drawing.Size(80, 59);
            this.btnKoma.TabIndex = 39;
            this.btnKoma.Text = ".";
            this.btnKoma.UseVisualStyleBackColor = false;
            this.btnKoma.Click += new System.EventHandler(this.btnKoma_click);
            // 
            // btnKurang
            // 
            this.btnKurang.BackColor = System.Drawing.Color.White;
            this.btnKurang.FlatAppearance.BorderSize = 0;
            this.btnKurang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKurang.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnKurang.ForeColor = System.Drawing.Color.DimGray;
            this.btnKurang.Location = new System.Drawing.Point(308, 328);
            this.btnKurang.Name = "btnKurang";
            this.btnKurang.Size = new System.Drawing.Size(80, 59);
            this.btnKurang.TabIndex = 38;
            this.btnKurang.Text = "-";
            this.btnKurang.UseVisualStyleBackColor = false;
            this.btnKurang.Click += new System.EventHandler(this.operationClick);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.White;
            this.btn3.FlatAppearance.BorderSize = 0;
            this.btn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn3.ForeColor = System.Drawing.Color.DimGray;
            this.btn3.Location = new System.Drawing.Point(226, 389);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(80, 59);
            this.btn3.TabIndex = 37;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn_click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.White;
            this.btn2.FlatAppearance.BorderSize = 0;
            this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn2.ForeColor = System.Drawing.Color.DimGray;
            this.btn2.Location = new System.Drawing.Point(144, 389);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(80, 59);
            this.btn2.TabIndex = 36;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn_click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.White;
            this.btn1.FlatAppearance.BorderSize = 0;
            this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn1.ForeColor = System.Drawing.Color.DimGray;
            this.btn1.Location = new System.Drawing.Point(62, 389);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(80, 59);
            this.btn1.TabIndex = 35;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn_click);
            // 
            // btnKali
            // 
            this.btnKali.BackColor = System.Drawing.Color.White;
            this.btnKali.FlatAppearance.BorderSize = 0;
            this.btnKali.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKali.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnKali.ForeColor = System.Drawing.Color.DimGray;
            this.btnKali.Location = new System.Drawing.Point(308, 267);
            this.btnKali.Name = "btnKali";
            this.btnKali.Size = new System.Drawing.Size(80, 59);
            this.btnKali.TabIndex = 34;
            this.btnKali.Text = "×";
            this.btnKali.UseVisualStyleBackColor = false;
            this.btnKali.Click += new System.EventHandler(this.operationClick);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.White;
            this.btn6.FlatAppearance.BorderSize = 0;
            this.btn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn6.ForeColor = System.Drawing.Color.DimGray;
            this.btn6.Location = new System.Drawing.Point(226, 328);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(80, 59);
            this.btn6.TabIndex = 32;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn_click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.White;
            this.btn5.FlatAppearance.BorderSize = 0;
            this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn5.ForeColor = System.Drawing.Color.DimGray;
            this.btn5.Location = new System.Drawing.Point(144, 328);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(80, 59);
            this.btn5.TabIndex = 31;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn_click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.White;
            this.btn4.FlatAppearance.BorderSize = 0;
            this.btn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn4.ForeColor = System.Drawing.Color.DimGray;
            this.btn4.Location = new System.Drawing.Point(62, 328);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(80, 59);
            this.btn4.TabIndex = 30;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn_click);
            // 
            // btnBagi
            // 
            this.btnBagi.BackColor = System.Drawing.Color.White;
            this.btnBagi.FlatAppearance.BorderSize = 0;
            this.btnBagi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBagi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnBagi.ForeColor = System.Drawing.Color.DimGray;
            this.btnBagi.Location = new System.Drawing.Point(308, 206);
            this.btnBagi.Name = "btnBagi";
            this.btnBagi.Size = new System.Drawing.Size(80, 59);
            this.btnBagi.TabIndex = 28;
            this.btnBagi.Text = "/";
            this.btnBagi.UseVisualStyleBackColor = false;
            this.btnBagi.Click += new System.EventHandler(this.operationClick);
            // 
            // btnPersen
            // 
            this.btnPersen.BackColor = System.Drawing.Color.White;
            this.btnPersen.FlatAppearance.BorderSize = 0;
            this.btnPersen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPersen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnPersen.ForeColor = System.Drawing.Color.DimGray;
            this.btnPersen.Location = new System.Drawing.Point(226, 206);
            this.btnPersen.Name = "btnPersen";
            this.btnPersen.Size = new System.Drawing.Size(80, 59);
            this.btnPersen.TabIndex = 27;
            this.btnPersen.Text = "%";
            this.btnPersen.UseVisualStyleBackColor = false;
            this.btnPersen.Click += new System.EventHandler(this.btnPersen_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.White;
            this.btn9.FlatAppearance.BorderSize = 0;
            this.btn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn9.ForeColor = System.Drawing.Color.DimGray;
            this.btn9.Location = new System.Drawing.Point(226, 267);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(80, 59);
            this.btn9.TabIndex = 26;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn_click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.White;
            this.btn8.FlatAppearance.BorderSize = 0;
            this.btn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn8.ForeColor = System.Drawing.Color.DimGray;
            this.btn8.Location = new System.Drawing.Point(144, 267);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(80, 59);
            this.btn8.TabIndex = 25;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn_click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.White;
            this.btn7.FlatAppearance.BorderSize = 0;
            this.btn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btn7.ForeColor = System.Drawing.Color.DimGray;
            this.btn7.Location = new System.Drawing.Point(62, 267);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(80, 59);
            this.btn7.TabIndex = 24;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn_click);
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.White;
            this.btnHapus.FlatAppearance.BorderSize = 0;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHapus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnHapus.ForeColor = System.Drawing.Color.DimGray;
            this.btnHapus.Location = new System.Drawing.Point(144, 206);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(80, 59);
            this.btnHapus.TabIndex = 22;
            this.btnHapus.Text = "←";
            this.btnHapus.UseVisualStyleBackColor = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // txtHasil
            // 
            this.txtHasil.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtHasil.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtHasil.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHasil.Location = new System.Drawing.Point(62, 120);
            this.txtHasil.MaxLength = 17;
            this.txtHasil.Multiline = true;
            this.txtHasil.Name = "txtHasil";
            this.txtHasil.Size = new System.Drawing.Size(325, 50);
            this.txtHasil.TabIndex = 43;
            this.txtHasil.Text = "0";
            this.txtHasil.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHasil.TextChanged += new System.EventHandler(this.txtHasil_TextChanged);
            // 
            // labelHasil
            // 
            this.labelHasil.AutoSize = true;
            this.labelHasil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHasil.Location = new System.Drawing.Point(317, 80);
            this.labelHasil.Name = "labelHasil";
            this.labelHasil.Size = new System.Drawing.Size(0, 20);
            this.labelHasil.TabIndex = 44;
            this.labelHasil.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnC
            // 
            this.btnC.BackColor = System.Drawing.Color.White;
            this.btnC.FlatAppearance.BorderSize = 0;
            this.btnC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.ForeColor = System.Drawing.Color.DimGray;
            this.btnC.Location = new System.Drawing.Point(62, 206);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(80, 59);
            this.btnC.TabIndex = 45;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = false;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnPlusMinus
            // 
            this.btnPlusMinus.BackColor = System.Drawing.Color.White;
            this.btnPlusMinus.FlatAppearance.BorderSize = 0;
            this.btnPlusMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlusMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnPlusMinus.ForeColor = System.Drawing.Color.DimGray;
            this.btnPlusMinus.Location = new System.Drawing.Point(62, 450);
            this.btnPlusMinus.Name = "btnPlusMinus";
            this.btnPlusMinus.Size = new System.Drawing.Size(80, 59);
            this.btnPlusMinus.TabIndex = 46;
            this.btnPlusMinus.Text = "+/-";
            this.btnPlusMinus.UseVisualStyleBackColor = false;
            this.btnPlusMinus.Click += new System.EventHandler(this.btnPlusMinus_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkOrange;
            this.label1.Location = new System.Drawing.Point(57, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 25);
            this.label1.TabIndex = 47;
            this.label1.Text = "Kalkulator Warung";
            // 
            // rtbHistory
            // 
            this.rtbHistory.BackColor = System.Drawing.Color.White;
            this.rtbHistory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHistory.Location = new System.Drawing.Point(488, 165);
            this.rtbHistory.Name = "rtbHistory";
            this.rtbHistory.ReadOnly = true;
            this.rtbHistory.Size = new System.Drawing.Size(193, 344);
            this.rtbHistory.TabIndex = 50;
            this.rtbHistory.Text = "";
            // 
            // labelHistory
            // 
            this.labelHistory.AutoSize = true;
            this.labelHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHistory.ForeColor = System.Drawing.Color.DimGray;
            this.labelHistory.Location = new System.Drawing.Point(528, 130);
            this.labelHistory.Name = "labelHistory";
            this.labelHistory.Size = new System.Drawing.Size(120, 18);
            this.labelHistory.TabIndex = 51;
            this.labelHistory.Text = "Tidak ada history";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(553, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 52;
            this.label3.Text = "History";
            // 
            // btnHapusHistory
            // 
            this.btnHapusHistory.BackColor = System.Drawing.Color.DarkOrange;
            this.btnHapusHistory.FlatAppearance.BorderSize = 0;
            this.btnHapusHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHapusHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHapusHistory.ForeColor = System.Drawing.Color.White;
            this.btnHapusHistory.Location = new System.Drawing.Point(488, 527);
            this.btnHapusHistory.Name = "btnHapusHistory";
            this.btnHapusHistory.Size = new System.Drawing.Size(193, 32);
            this.btnHapusHistory.TabIndex = 53;
            this.btnHapusHistory.Text = "Hapus History";
            this.btnHapusHistory.UseVisualStyleBackColor = false;
            this.btnHapusHistory.Click += new System.EventHandler(this.btnHapusHistory_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 551);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 52);
            this.label2.TabIndex = 54;
            this.label2.Text = "By:\r\n\r\nFraiza Geraldi Alghifary (0617104026)\r\nTeknik Informatika B - B2\r\n";
            // 
            // KalkulatorWarungV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 651);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnHapusHistory);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelHistory);
            this.Controls.Add(this.rtbHistory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPlusMinus);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.labelHasil);
            this.Controls.Add(this.txtHasil);
            this.Controls.Add(this.btnSamaDengan);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btnTambah);
            this.Controls.Add(this.btnKoma);
            this.Controls.Add(this.btnKurang);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btnKali);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btnBagi);
            this.Controls.Add(this.btnPersen);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btnHapus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(789, 690);
            this.MinimumSize = new System.Drawing.Size(420, 690);
            this.Name = "KalkulatorWarungV2";
            this.Text = "Kalkulator Warung V2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSamaDengan;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnKoma;
        private System.Windows.Forms.Button btnKurang;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btnKali;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btnBagi;
        private System.Windows.Forms.Button btnPersen;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.TextBox txtHasil;
        private System.Windows.Forms.Label labelHasil;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnPlusMinus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbHistory;
        private System.Windows.Forms.Label labelHistory;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnHapusHistory;
        private System.Windows.Forms.Label label2;
    }
}

